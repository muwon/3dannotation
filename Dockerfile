FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive

WORKDIR /root

COPY run.sh .

RUN sed -i 's|archive.ubuntu.com|mirrors.aliyun.com|g' /etc/apt/sources.list &&\
    sed -i 's|security.ubuntu.com|mirrors.aliyun.com|g' /etc/apt/sources.list &&\
    apt update && apt install -y python3-pip git wget

RUN pip install --upgrade pip -i http://mirrors.aliyun.com/pypi/simple --trusted-host mirrors.aliyun.com

COPY . SUSTechPOINTS

RUN cd SUSTechPOINTS && \
        #wget https://github.com/naurril/SUSTechPOINTS/releases/download/0.1/deep_annotation_inference.h5 -P algos/models && \
        wget http://webserver.pegasus.tech/datasets/SUSTechPOINTS/algos/models/deep_annotation_inference.h5 -P algos/models && \
        python3 -m pip install -r ./requirement.txt -i http://mirrors.aliyun.com/pypi/simple --trusted-host mirrors.aliyun.com

ENTRYPOINT ["/root/run.sh"]

EXPOSE 8081
