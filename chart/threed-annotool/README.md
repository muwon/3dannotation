# 3D Annotation Deployment





## Procedure
1. Set K8s context

   ```
   kubectl config use <k8s-context-name>
   ```

2. Create namespace

   ```
   cd ~/Work/pegasus/SUSTechPOINTS/chart
   kubectl apply -f threed-annotool/threed-annotool-ns.yaml
   ```

3. Deploy

   ```
   helm upgrade --install --dry-run --debug --namespace threed-annotool threed-annotool ./threed-annotool > ~/3d-annotool-k8s-dev-shanghai-CURRENT-012322.yaml
   helm upgrade --install --debug --namespace threed-annotool threed-annotool ./threed-annotool
   ```

4. Create DNS record for Ingress

   ```
   kubectl get -nthreed-annotool ingress
   
   NAME               CLASS    HOSTS                    ADDRESS                       PORTS   AGE
   threed-annotool-ing   <none>   pega3dlabel.pegasus.tech   192.168.16.15,192.168.16.16,192.168.16.17,192.168.16.18,192.168.16.19,192.168.16.20   80, 443   42s
   ```

